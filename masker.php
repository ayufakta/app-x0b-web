<?php
$DB_NAME = "kampus";
    $DB_USER = "root";
    $DB_PASS = "";
    $DB_SERVER_LOC = "localhost";

    $conn = mysqli_connect($DB_SERVER_LOC,$DB_USER,$DB_PASS,$DB_NAME);
    $sql =  "SELECT m.nim,m.nama,p.id_prodi, m.photos, m.alamat , j.kelamin
           FROM mahasiswa m,prodi p , jenis_kelamin j
        WHERE m.id_prodi=p.id_prodi and m.id_jk = j.id_jk";
			
    $result = mysqli_query($conn,$sql);
?>

<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>KATALOG MASKER</title>
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
            aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="masker.php">Data Masker</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="jenis.php">Data Jenis Masker</a>
                </li>
            </ul>
        </div>
    </nav>
    <br>
    <div class="container">
        <h1> KATALOG MASKER</h1>
        <div class="table-responsive">
            <table class="table table-bordered table-striped bg-light">
                <thead class="table-striped table-grey" style="text-align: center;">
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Kode</th>
                        <th scope="col">Nama Masker</th>
                        <th scope="col">Jenis Masker</th>
                        <th scope="col">Photo</th>
                        <th scope="col">Deskripsi Masker</th>
                        <th scope="col">Berat Masker</th>
                    </tr>
                </thead>
                <?php
                    $no=1;
                    while($mahasiswa = mysqli_fetch_assoc($result)){
                ?>
                <tbody>
                    <tr>
                        <th scope="row" style="text-align: center;"><?php echo $no;$no++; ?></th>
                        <td><?php echo $mahasiswa['nim']; ?></td>
                        <td><?php echo $mahasiswa['nama']; ?></td>
						<td><?php echo $mahasiswa['id_prodi']; ?></td>
                        <td style="text-align: center;">
                            <img src="images/<?php echo $mahasiswa['photos']; ?>" style="width: 200px;" alt="Foto Mahasiswa">
                        </td>
						<td><?php echo $mahasiswa['alamat']; ?></td>
						<td><?php echo $mahasiswa['kelamin']; ?></td>
                    </tr>
                    </tr>
                </tbody>
                <?php } ?>
            </table>
        </div>
    </div>

</body>

</html>