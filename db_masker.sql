-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 15 Bulan Mei 2020 pada 15.04
-- Versi server: 10.1.38-MariaDB
-- Versi PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `masker`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `berat`
--

CREATE TABLE `berat` (
  `id_berat` int(11) NOT NULL,
  `beratmasker` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `berat`
--

INSERT INTO `berat` (`id_berat`, `beratmasker`) VALUES
(1, 'Masker Bubuk'),
(2, 'Masker Organik'),
(3, 'Sheet Mask'),
(4, 'Masker Komedo');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenis_masker`
--

CREATE TABLE `jenis_masker` (
  `id_j` int(11) NOT NULL,
  `jenis` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jenis_masker`
--

INSERT INTO `jenis_masker` (`id_j`, `jenis`) VALUES
(1, '15gr'),
(2, '20gr'),
(3, '45gr'),
(4, '60gr'),
(5, '75gr'),
(6, '100gr'),
(7, '1kg'),
(8, '5kg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `masker`
--

CREATE TABLE `masker` (
  `kode` varchar(10) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `id_j` int(11) NOT NULL,
  `photos` varchar(20) DEFAULT NULL,
  `deskripsi` varchar(60) NOT NULL,
  `id_berat` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `masker`
--

INSERT INTO `masker` (`kode`, `nama`, `id_j`, `photos`, `deskripsi`, `id_berat`) VALUES
('101010', 'Masker Bubuk', 3, 'puluh.jpg', 'Sheet Mask Glowing', 1),
('111111', 'Masker Organik Kode Baru', 4, 'satu.png', 'Masker Organik Varian Campur', 2),
('222222', 'Masker Bubuk', 1, 'dua.jpg', 'Masker Bubuk Beras', 2),
('444444', 'Organik', 1, 'empat.jpg', 'Masker Mantul', 2),
('555555', 'Glowing', 2, 'lima.jpg', 'Masker Enak', 1),
('666666', 'Masker Wangi', 2, 'enam.jpg', 'Masker Varian Anggur', 1),
('777777', 'Masker Cantik', 3, 'tujuh.jpg', 'Masker Varian Apel', 1),
('888888', 'Masker', 2, 'lhapan.jpg', 'Masker Varian Jeruk', 2),
('999999', 'Masker', 1, 'mbilan.jpg', 'Masker Bikin Cantik', 1);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `berat`
--
ALTER TABLE `berat`
  ADD PRIMARY KEY (`id_berat`);

--
-- Indeks untuk tabel `jenis_masker`
--
ALTER TABLE `jenis_masker`
  ADD PRIMARY KEY (`id_j`);

--
-- Indeks untuk tabel `masker`
--
ALTER TABLE `masker`
  ADD PRIMARY KEY (`kode`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
